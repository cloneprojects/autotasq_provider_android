package com.app.autotasqpro.FCM;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.app.autotasqpro.Events.Status;
import com.app.autotasqpro.R;
import com.app.autotasqpro.activities.MainActivity;


import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

/**
 * Created by user on 25-10-2017.
 */

public class FcmMessageService extends FirebaseMessagingService {

    private static final String TAG = FcmMessageService.class.getSimpleName();



    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        JSONObject jsonObject = new JSONObject(remoteMessage.getData());

        sendNotification(jsonObject);
        EventBus.getDefault().post(new Status());


    }

    private void sendNotification(JSONObject jsonObject) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);



        Notification notification =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.notification_icon)
                        .setContentTitle(jsonObject.optString("title"))
                        .setContentText(jsonObject.optString("message"))
                        .setAutoCancel(true)
                        .setChannel("values")
                        .setContentIntent(pendingIntent).build();


        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(0, notification);


    }


}