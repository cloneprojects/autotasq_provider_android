package com.app.autotasqpro.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;


import com.app.autotasqpro.R;
import com.app.autotasqpro.Volley.ApiCall;
import com.app.autotasqpro.Volley.VolleyCallback;
import com.app.autotasqpro.helpers.UrlHelper;
import com.app.autotasqpro.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {
    Button sendOtp;
    EditText emailEditText;
    ImageView backButton;

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }

        initViews();
        initListners();
    }

    private void initListners() {
        backButton.setOnClickListener(this);
        sendOtp.setOnClickListener(this);

    }

    private void initViews() {
        backButton = (ImageView) findViewById(R.id.backButton);
        sendOtp = (Button) findViewById(R.id.sendOtp);
        emailEditText = (EditText) findViewById(R.id.emailEditText);

    }

    @Override
    public void onClick(View view) {

        if (view == backButton) {
            finish();
        } else if (view == sendOtp) {
            if (isValidInputs().equalsIgnoreCase("true")) {
                requestOtp();
            } else {
                Utils.toast(ForgotPasswordActivity.this, isValidInputs());
            }
        }
    }

    private String isValidInputs() {

        String val;
        if (emailEditText.getText().length() == 0 || !isValidEmail(emailEditText.getText().toString())) {
            val = getResources().getString(R.string.please_enter_valid_email);
        } else {
            val = "true";
        }
        return val;
    }

    private void requestOtp() {
        ApiCall.PostMethod(ForgotPasswordActivity.this, UrlHelper.FORGOT_PASSWORD, getInputs(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                String otp = response.optString("otp");
                moveVerfication(otp);
            }


        });

    }

    private void moveVerfication(String otp) {
        Intent intent = new Intent(ForgotPasswordActivity.this, EnterVerificationCodeActivity.class);
        intent.putExtra("otp", otp);
        intent.putExtra("emailValue", emailEditText.getText().toString());
        startActivity(intent);
    }

    private JSONObject getInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", emailEditText.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
